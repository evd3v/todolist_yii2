<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-4">
            <textarea class="form-control task-description" placeholder="Write your description..." rows="3"></textarea>
            <div class="select-and-button row" style="margin-top: 10px">
                <div class="col-md-8">
                    <select class="task-select form-control" name="task-select">
                        <option value="label-danger" class="High">High</option>
                        <option value="label-warning" class="Middle">Middle</option>
                        <option value="label-success" class="Low">Low</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-success task-button" onclick="addElement()">Create a task
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <ul class="list-of-tasks">

            </ul>
        </div>
    </div>

</div>